package com.example.bookappwithnavigation.data.local

import com.example.bookappwithnavigation.model.Book
import javax.inject.Inject

class BookLab @Inject constructor() {

    val thrillerList by lazy {

        val list = mutableListOf<Book>()

        list.add(
            Book(
                "1542044197",
                "The Girl in Room 105",
                "Chetan Bhagat",
                "Crime, Thriller & Mystery",
                "https://images-eu.ssl-images-amazon.com/images/I/413Ge3dy3qL.jpg"
            )
        )

        list.add(
            Book(
                "1489417540",
                "Sherlock Holmes: The Definitive Collection",
                "Stephen Fry",
                "Crime, Thriller & Mystery",
                "https://images-na.ssl-images-amazon.com/images/I/519q0gOMn9L._SX342_.jpg"
            )
        )

        list.add(
            Book(
                "9780007428540",
                "A Game of Thrones",
                "George R. R. Martin",
                "Crime, Thriller & Mystery",
                "https://m.media-amazon.com/images/I/51HoTTx2ZhL._AC_UL436_.jpg"
            )
        )

        list.add(
            Book(
                "1899888233",
                "And Then There Were None",
                "Agatha Christie",
                "Crime, Thriller & Mystery",
                "https://images-na.ssl-images-amazon.com/images/I/51%2BkE0KESsL._SX326_BO1,204,203,200_.jpg"
            )
        )

        list.add(
            Book(
                "0007580916",
                "The World of Ice and Fire (Song of Ice & Fire)",
                "George R.R. Martin",
                "Crime, Thriller & Mystery",
                "https://images-na.ssl-images-amazon.com/images/I/51VtdgQW6RL._SX375_BO1,204,203,200_.jpg"
            )
        )

        list
    }

    val actionList by lazy {

        val list = mutableListOf<Book>()

        list.add(
            Book(
                "0553328255",
                "Sherlock Holmes: The Truly Complete Collection",
                "Arthur Conan Doyle",
                "Action",
                "https://images-eu.ssl-images-amazon.com/images/I/41smCKvDmYL.jpg"
            )
        )

        list.add(
            Book(
                "9382618341",
                "The Oath of the Vayuputras",
                "Amish Tripathi",
                "Action",
                "https://images-na.ssl-images-amazon.com/images/I/51P5DufDtVL._SX323_BO1,204,203,200_.jpg"
            )
        )

        list.add(
            Book(
                "3849675807",
                "Treasure Island",
                "Robert Louis Stevenson",
                "Action",
                "https://images-eu.ssl-images-amazon.com/images/I/416nuQYuaIL.jpg"
            )
        )

        list
    }

    val educationList by lazy {
        val list = mutableListOf<Book>()

        list.add(
            Book(
                "1788474546",
                "Kotlin Programming",
                "Iyanu Adelekan",
                "Education",
                "https://images-na.ssl-images-amazon.com/images/I/41aeHqW-3lL._SX403_BO1,204,203,200_.jpg"
            )
        )

        list.add(
            Book(
                "9352135555",
                "Android Cookbook: Problems and Solutions for Android Developers",
                "Ian F. Darwin",
                "Education",
                "https://images-na.ssl-images-amazon.com/images/I/511dk2EXCtL._SX386_BO1,204,203,200_.jpg"
            )
        )

        list.add(
            Book(
                "0984782850",
                "Cracking the Coding Interview: 189 Programing Questions and Solutions",
                "Gayle Laakmann McDowell",
                "Education",
                "https://images-na.ssl-images-amazon.com/images/I/51l5XzLln%2BL._SX348_BO1,204,203,200_.jpg"
            )
        )

        list.add(
            Book(
                "9780062413406",
                "Alibaba: The House that Jack Ma Built",
                "Duncan Clark",
                "Education",
                "https://images-na.ssl-images-amazon.com/images/I/41%2Bc-nA7hYL._SX329_BO1,204,203,200_.jpg"
            )
        )

        list
    }

    val romanceList by lazy {
        val list = mutableListOf<Book>()

        list.add(
            Book(
                "9388810449",
                "The Greatest Short Stories of Leo Tolstoy",
                "Leo Tolstoy",
                "Romance",
                "https://images-eu.ssl-images-amazon.com/images/I/51zutbKzsOL.jpg"
            )
        )

        list.add(
            Book(
                "0143426591",
                "The Perfect Us",
                "Durjoy Datta",
                "Romance",
                "https://images-na.ssl-images-amazon.com/images/I/41ch88E5cCL._SX324_BO1,204,203,200_.jpg"
            )
        )

        list.add(
            Book(
                "9382665889",
                "You are My Reason to Smile",
                "Arpit Vageria",
                "Romance",
                "https://images-na.ssl-images-amazon.com/images/I/513%2BeLvI8ML._SX327_BO1,204,203,200_.jpg"
            )
        )
        list

    }

    val sciFiList by lazy {

        val list = mutableListOf<Book>()

        list.add(
            Book(
                "9781408855652",
                "Harry Potter and the Philosopher's Stone",
                "J.K. Rowling",
                "Sci-Fi",
                "https://images-na.ssl-images-amazon.com/images/I/51ifu1aebKL._SX332_BO1,204,203,200_.jpg"
            )
        )

        list.add(
            Book(
                "9353024668",
                "The Last Avatar (Age of Kalki #1)",
                "Vishwas Mudagal",
                "Sci-Fi",
                "https://images-na.ssl-images-amazon.com/images/I/51V2Vd2Sc8L._SX324_BO1,204,203,200_.jpg"
            )
        )

        list.add(
            Book(
                "0552997773",
                "The Sparrow",
                "Mary Doria Russell",
                "Sci-Fi",
                "https://images-na.ssl-images-amazon.com/images/I/51TCJR6ztjL._SX319_BO1,204,203,200_.jpg"
            )
        )

        list.add(
            Book(
                "1401248861",
                "Batman: Arkham Origins",
                "Ernest Hemingway",
                "Sci-Fi",
                "https://images-na.ssl-images-amazon.com/images/I/61io953g8UL._SX336_BO1,204,203,200_.jpg"
            )
        )

        list.add(
            Book(
                "1785659758",
                "Marvel's SPIDER-MAN: Hostile Takeove",
                "David Liss",
                "Sci-Fi",
                "https://images-na.ssl-images-amazon.com/images/I/51wqQzOjrRL._SX303_BO1,204,203,200_.jpg"
            )
        )

        list.add(
            Book(
                "1785659561",
                "Avengers: Everybody Wants to Rule the World: A Novel of the Marvel Universe: 1",
                "Dan Abnett",
                "Sci-Fi",
                "https://images-na.ssl-images-amazon.com/images/I/51f4GSVDLKL._SX303_BO1,204,203,200_.jpg"
            )
        )
        list
    }

}