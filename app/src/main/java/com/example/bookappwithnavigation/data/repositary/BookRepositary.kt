package com.example.bookappwithnavigation.data.repositary

import android.content.Context
import com.example.bookappwithnavigation.R
import com.example.bookappwithnavigation.data.local.BookLab
import com.example.bookappwithnavigation.data.local.CategoryLab
import com.example.bookappwithnavigation.model.Book
import java.lang.IllegalArgumentException
import javax.inject.Inject

class BookRepositary @Inject constructor(
//    private val bookLab: BookLab,
//    private val context: Context,
    private val categoryLab: CategoryLab
) {

    /*fun getBooks(type: String): List<Book> {
        return when (type) {
            R.string.action -> bookLab.actionList
            R.string.romance -> bookLab.romanceList
            R.string.thriller -> bookLab.thrillerList
            R.string.education -> bookLab.educationList
            R.string.scifi -> bookLab.sciFiList

            else -> throw IllegalArgumentException("Illegal Argument")
        }
    }*/

    fun getBooks(categoryName: String): List<Book> {
        val foundCategory = categoryLab.categoryList.find {
            it.name == categoryName
        }
        return foundCategory?.bookList ?: listOf()
    }
}