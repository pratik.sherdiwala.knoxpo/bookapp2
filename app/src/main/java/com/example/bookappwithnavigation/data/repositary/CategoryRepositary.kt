package com.example.bookappwithnavigation.data.repositary

import com.example.bookappwithnavigation.data.local.CategoryLab
import com.example.bookappwithnavigation.model.Category
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CategoryRepositary @Inject constructor(private val categoryLab: CategoryLab) {

    private val categoryListSubject = BehaviorSubject.createDefault<List<Category>>(categoryLab.categoryList)

    /*fun getCategoryList(): List<Category> {
        return categoryLab.categoryList
    }*/

    fun getCategories(): Observable<List<Category>> {
        return categoryListSubject
    }

    fun addCategory(category: Category) {
        categoryLab.addCategory(category)
        categoryListSubject.onNext(categoryLab.categoryList)
    }
}