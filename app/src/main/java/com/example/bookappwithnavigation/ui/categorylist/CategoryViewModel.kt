package com.example.bookappwithnavigation.ui.categorylist

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.bookappwithnavigation.Event
import com.example.bookappwithnavigation.data.repositary.CategoryRepositary
import com.example.bookappwithnavigation.model.Category
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class CategoryViewModel @Inject constructor(private val categoryRepositary: CategoryRepositary) : ViewModel() {

    val itemList = MutableLiveData<List<Category>>()

    private val disposable = CompositeDisposable()

//    fun getCategory() {
//        itemList.value = categoryRepositary.getCategoryList()
//    }

    fun fetchCategories() {
        categoryRepositary.getCategories()
            .subscribe(
                {
                    itemList.value = it
                },
                {

                },
                {

                }

            ).also {
                disposable.add(it)
            }
    }

    private val _openTaskEvent = MutableLiveData<Event<String>>()
    val openTaskEvent: LiveData<Event<String>>
        get() = _openTaskEvent

    fun openTask(id: String) {
        _openTaskEvent.value = Event(id)
    }

}