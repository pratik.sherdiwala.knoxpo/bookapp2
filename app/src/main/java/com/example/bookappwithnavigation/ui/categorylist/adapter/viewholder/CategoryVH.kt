package com.example.bookappwithnavigation.ui.categorylist.adapter.viewholder

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.bookappwithnavigation.R
import com.example.bookappwithnavigation.model.Category

class CategoryVH(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private val mName = itemView.findViewById<TextView>(R.id.nameTV)
    private val mId = itemView.findViewById<TextView>(R.id.idTV)

    fun bindCategory(
        category: Category,
        categoryClick: (category: Category) -> Unit
    ) {

        mName.text = category.name
        mId.text = category.id

        itemView.setOnClickListener {
            categoryClick(category)
        }

    }

}