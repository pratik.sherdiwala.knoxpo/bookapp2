package com.example.bookappwithnavigation.ui.categorydetail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.bookappwithnavigation.Event
import com.example.bookappwithnavigation.data.repositary.CategoryRepositary
import com.example.bookappwithnavigation.model.Category
import javax.inject.Inject

class CategoryDetailViewModel @Inject constructor(private val categoryRepositary: CategoryRepositary) : ViewModel() {

    val id = MutableLiveData<String>()
    val name = MutableLiveData<String>()

    private val _categoryAddedEvent = MutableLiveData<Event<Unit>>()

    val categoryAddedEvent: LiveData<Event<Unit>>
        get() = _categoryAddedEvent

    fun addCategory(){
        val category=Category(
            name=name.value!!,
            id=id.value!!
        )
        categoryRepositary.addCategory(category)
        _categoryAddedEvent.value=Event(Unit)
    }
}