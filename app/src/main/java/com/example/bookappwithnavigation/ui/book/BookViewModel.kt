package com.example.bookappwithnavigation.ui.book

import android.util.EventLog
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.bookappwithnavigation.Event
import com.example.bookappwithnavigation.data.repositary.BookRepositary
import com.example.bookappwithnavigation.data.repositary.CategoryRepositary
import com.example.bookappwithnavigation.model.Category
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class BookViewModel @Inject constructor(private val categoryRepositary: CategoryRepositary) : ViewModel() {

    val disposable = CompositeDisposable()

    val commandShow = MutableLiveData<Event<List<Category>>>()

    fun fetchCategories() {

        categoryRepositary.getCategories()
            .subscribe(
                {
                    commandShow.value = Event(it)
                },
                {

                },
                {

                }
            )
            .also {
                disposable.add(it)
            }

    }


}