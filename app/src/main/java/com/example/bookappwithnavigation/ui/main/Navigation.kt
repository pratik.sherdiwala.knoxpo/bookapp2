package com.example.bookappwithnavigation.ui.main

interface Navigation {

    fun navigateBook()

    fun navigateCategory()
}