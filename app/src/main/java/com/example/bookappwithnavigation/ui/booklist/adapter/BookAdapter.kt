package com.example.bookappwithnavigation.ui.booklist.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.bookappwithnavigation.R
import com.example.bookappwithnavigation.model.Book
import com.example.bookappwithnavigation.ui.booklist.adapter.viewholder.BookVH

class BookAdapter : RecyclerView.Adapter<BookVH>() {

    private var items: List<Book>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookVH {
        return BookVH(
            LayoutInflater.from(
                parent.context
            )
                .inflate(
                    R.layout.item_list_book,
                    parent,
                    false
                )
        )
    }

    override fun getItemCount() = items?.size ?: 0

    override fun onBindViewHolder(holder: BookVH, position: Int) {
        holder.bindBook(items!![position])
    }

    fun updateBook(newBook: List<Book>) {
        items = newBook
        notifyDataSetChanged()
    }
}