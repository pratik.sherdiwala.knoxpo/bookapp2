package com.example.bookappwithnavigation.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.bookappwithnavigation.Event
import javax.inject.Inject

class MainViewModel @Inject constructor() : ViewModel() {

    private val _showBookEvent = MutableLiveData<Event<Unit>>()
    val showBookEvent:LiveData<Event<Unit>>
        get() = _showBookEvent

    fun openBookFragment(){
        _showBookEvent.value= Event(Unit)
    }

    private val _showCategoryEvent=MutableLiveData<Event<Unit>>()
    val showCategoryEvent:LiveData<Event<Unit>>
        get() = _showCategoryEvent

    fun openCategoryEvent(){
        _showCategoryEvent.value=Event(Unit)
    }

}