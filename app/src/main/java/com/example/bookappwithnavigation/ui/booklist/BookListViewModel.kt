package com.example.bookappwithnavigation.ui.booklist

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.bookappwithnavigation.data.repositary.BookRepositary
import com.example.bookappwithnavigation.model.Book
import javax.inject.Inject

class BookListViewModel @Inject constructor(private val bookRepositary: BookRepositary) : ViewModel() {

    val itemList = MutableLiveData<List<Book>>()

    fun getBook(type: String) {
        itemList.value = bookRepositary.getBooks(type)
    }
}