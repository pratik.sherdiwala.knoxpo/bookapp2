package com.example.bookappwithnavigation.ui.book

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.viewpager.widget.ViewPager
import com.example.bookappwithnavigation.R
import com.example.bookappwithnavigation.databinding.FragmentBookBinding
import com.example.bookappwithnavigation.model.Book
import com.example.bookappwithnavigation.model.Category
import com.example.bookappwithnavigation.ui.booklist.BookListFragment
import com.example.bookappwithnavigation.viewmodel.AppViewModelFactory
import com.google.android.material.tabs.TabLayout
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

class BookFragment : Fragment(), HasSupportFragmentInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector() = dispatchingAndroidInjector

    private var itemList: List<Category>? = null

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: FragmentBookBinding

    private val viewModel by lazy {
        ViewModelProviders.of(
            this,
            viewModelFactory
        )
            .get(BookViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidSupportInjection.inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val fragment = this

        with(binding) {
            viewModel = fragment.viewModel
            tabLayout.setupWithViewPager(viewPager)
            tabLayout.tabMode = TabLayout.MODE_SCROLLABLE
        }

        with(viewModel) {

            commandShow.observe(fragment,
                Observer {
                    it.getContentIfNothandled()?.let {
                        itemList = it
                        setUpViewPager(binding.viewPager)
                    }
                })
            fetchCategories()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        this.binding = FragmentBookBinding.inflate(
            inflater,
            container,
            false
        )

        binding.lifecycleOwner = this
        return binding.root

    }

    private fun setUpViewPager(viewPager: ViewPager) {

        val adapter = ViewPagerAdapter(childFragmentManager)

       /* adapter.addFragment(BookListFragment.newInstance(R.string.action), getString(R.string.action))
        adapter.addFragment(BookListFragment.newInstance(R.string.scifi), getString(R.string.scifi))
        adapter.addFragment(BookListFragment.newInstance(R.string.education), getString(R.string.education))
        adapter.addFragment(BookListFragment.newInstance(R.string.thriller), getString(R.string.thriller))
        adapter.addFragment(BookListFragment.newInstance(R.string.romance), getString(R.string.romance))
        */

        for(category in itemList!!){
            adapter.addFragment(BookListFragment.newInstance(category.name),category.name)
        }

        viewPager.adapter = adapter
    }
}