package com.example.bookappwithnavigation.ui._common

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.bookappwithnavigation.model.Book
import com.example.bookappwithnavigation.model.Category
import com.example.bookappwithnavigation.ui.booklist.adapter.BookAdapter
import com.example.bookappwithnavigation.ui.categorylist.adapter.CategoryAdapter

object DataBindingAdapter {

    @JvmStatic
    @BindingAdapter("items")
    fun setBook(view: RecyclerView, items: List<Book>) {
        items?.let {
            (view.adapter as? BookAdapter)?.updateBook(it)
        }
    }

    @JvmStatic
    @BindingAdapter("items")
    fun setCategory(view: RecyclerView, items: List<Category>) {
        items.let {
            (view.adapter as? CategoryAdapter)?.updateCategory(it)
        }
    }
}