package com.example.bookappwithnavigation.ui.categorydetail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.example.bookappwithnavigation.R
import com.example.bookappwithnavigation.databinding.ActivityEditCategoryBinding
import com.example.bookappwithnavigation.ui._common.DataBindingActivity
import com.example.bookappwithnavigation.viewmodel.AppViewModelFactory
import dagger.android.AndroidInjection
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.activity_edit_category.*
import javax.inject.Inject

class CategoryDetailActivity : DataBindingActivity<ActivityEditCategoryBinding>() {

    companion object {
        private val TAG = CategoryDetailActivity::class.java.simpleName

        private const val ACTION_ADD = 0

        private val EXTRA_ACTION = "$TAG.EXTRA_ACTION"

        internal fun intentForAddCategory(context: Context): Intent {
            return Intent(
                context,
                CategoryDetailActivity::class.java
            )
                .apply {
                    putExtra(EXTRA_ACTION, ACTION_ADD)
                }
        }
    }


    override val layoutID = R.layout.activity_edit_category

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by lazy {
        ViewModelProviders.of(
            this,
            viewModelFactory
        )
            .get(CategoryDetailViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        val activity = this

        with(binding) {
            viewModel = activity.viewModel
        }

        with(viewModel) {
            categoryAddedEvent.observe(
                activity,
                Observer {
                    it.getContentIfNothandled()?.let {
                        finish()
                    }
                }
            )
        }

        when(intent.getIntExtra(EXTRA_ACTION,-1)){
            ACTION_ADD -> {
                updateBTN.setOnClickListener {
                    addCategory()
                }
                updateBTN.text="CREATE"
            }
        }
    }

    private fun addCategory() {
        with(viewModel) {
            addCategory()
        }
    }

}