package com.example.bookappwithnavigation.ui.book

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class ViewPagerAdapter(fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager) {

    private val fragment = ArrayList<Fragment>()
    private val fragmentTitles = ArrayList<String>()

    fun addFragment(fm: Fragment, title: String) {
        fragment.add(fm)
        fragmentTitles.add(title)
    }

    override fun getItem(position: Int) = fragment[position]

    override fun getCount() = fragment.size

    override fun getPageTitle(position: Int) = fragmentTitles[position]
}