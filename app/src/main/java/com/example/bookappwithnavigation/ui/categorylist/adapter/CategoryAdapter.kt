package com.example.bookappwithnavigation.ui.categorylist.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.bookappwithnavigation.R
import com.example.bookappwithnavigation.model.Category
import com.example.bookappwithnavigation.ui.categorylist.CategoryViewModel
import com.example.bookappwithnavigation.ui.categorylist.adapter.viewholder.CategoryVH

class CategoryAdapter constructor(private val categoryViewModel: CategoryViewModel) :
    RecyclerView.Adapter<CategoryVH>() {

    private var items: List<Category>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryVH {
        return CategoryVH(
            LayoutInflater.from(parent.context)
                .inflate(
                    R.layout.item_list_category,
                    parent,
                    false
                )
        )
    }

    override fun getItemCount() = items?.size ?: 0

    override fun onBindViewHolder(holder: CategoryVH, position: Int) {
        holder.bindCategory(items!![position],
            categoryClick = {
                categoryViewModel.openTask(it.id)
            })
    }

    fun updateCategory(newCategory: List<Category>) {
        items = newCategory
        notifyDataSetChanged()
    }
}