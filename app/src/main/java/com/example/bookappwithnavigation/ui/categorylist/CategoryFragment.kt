package com.example.bookappwithnavigation.ui.categorylist

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.bookappwithnavigation.R
import com.example.bookappwithnavigation.TaskNavigator
import com.example.bookappwithnavigation.databinding.FragmentCategoryBinding
import com.example.bookappwithnavigation.ui.categorydetail.CategoryDetailActivity
import com.example.bookappwithnavigation.ui.categorylist.adapter.CategoryAdapter
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_category.*
import kotlinx.android.synthetic.main.item_list_category.*
import javax.inject.Inject

class CategoryFragment : Fragment(), TaskNavigator {


    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: FragmentCategoryBinding

    private val viewModel by lazy {
        ViewModelProviders.of(
            this,
            viewModelFactory
        )
            .get(CategoryViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidSupportInjection.inject(this)
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val fragment = this

        with(binding) {
            viewModel = fragment.viewModel

            categoryRV.layoutManager = LinearLayoutManager(context)
            categoryRV.adapter = CategoryAdapter(fragment.viewModel)
        }

        viewModel.fetchCategories()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        this.binding = FragmentCategoryBinding.inflate(
            inflater,
            container,
            false
        )
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.item_category, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.addBTN -> {
                addNewTask()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun addNewTask() {
        startActivity(
            CategoryDetailActivity.intentForAddCategory(context!!)
        )
    }

}