package com.example.bookappwithnavigation.ui.booklist.adapter.viewholder

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.bookappwithnavigation.R
import com.example.bookappwithnavigation.model.Book
import kotlinx.android.synthetic.main.item_list_book.view.*

class BookVH(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private val mNameTV = itemView.findViewById<TextView>(R.id.nameTV)
    private val mIdTV = itemView.findViewById<TextView>(R.id.idTV)
    private val mAuthorTV = itemView.findViewById<TextView>(R.id.authorTV)
    private val mCategory = itemView.findViewById<TextView>(R.id.genreTV)

    fun bindBook(book: Book) {
        mNameTV.text = book.name
        mIdTV.text = book.id
        mAuthorTV.text = book.author
        mCategory.text = book.genre
    }
}