package com.example.bookappwithnavigation.ui.booklist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentFactory
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.bookappwithnavigation.databinding.FragmentBookListBinding
import com.example.bookappwithnavigation.ui.booklist.adapter.BookAdapter
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

class BookListFragment : Fragment() {

    companion object {

        private val TAG = BookListFragment::class.java.simpleName
        private val ARGS_TITLE = "$TAG.ERGS_TITLE"

        fun newInstance(title: String) = BookListFragment().apply {
            arguments = Bundle().apply {
                putString(ARGS_TITLE, title)
            }
        }
    }

    private val title: String
        get() {
            return arguments!!.getString(ARGS_TITLE)!!
        }


    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: com.example.bookappwithnavigation.databinding.FragmentBookListBinding

    private val viewModel by lazy {
        ViewModelProviders.of(
            this,
            viewModelFactory
        )
            .get(BookListViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidSupportInjection.inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val fragment = this

        with(binding) {
            viewModel = fragment.viewModel

            itemsRV.layoutManager = LinearLayoutManager(context)
            itemsRV.adapter = BookAdapter()
        }
        viewModel.getBook(title)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        this.binding = FragmentBookListBinding.inflate(
            inflater,
            container,
            false
        )

        binding.lifecycleOwner = this
        return binding.root
    }

}