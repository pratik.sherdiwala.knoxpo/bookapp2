package com.example.bookappwithnavigation

open class Event<out T>(private val content: T) {

    var hasBeenHandled = false
        private set

    fun getContentIfNothandled(): T? {
        return if (hasBeenHandled) {
            null
        } else {
            hasBeenHandled = true
            content
        }
    }

    fun peekContent(): T = content
}