package com.example.bookappwithnavigation.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.bookappwithnavigation.ui.book.BookViewModel
import com.example.bookappwithnavigation.ui.booklist.BookListViewModel
import com.example.bookappwithnavigation.ui.categorydetail.CategoryDetailViewModel
import com.example.bookappwithnavigation.ui.categorylist.CategoryViewModel
import com.example.bookappwithnavigation.viewmodel.AppViewModelFactory
import com.example.bookappwithnavigation.ui.main.MainViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindMainViewModel(mainViewModel: MainViewModel): ViewModel

    @Binds
    abstract fun provideViewModelFactory(appViewModelFactory: AppViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(BookViewModel::class)
    abstract fun bindBookViewModel(bookViewModel: BookViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(BookListViewModel::class)
    abstract fun bindBookListViewModel(bookListViewModel: BookListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CategoryViewModel::class)
    abstract fun bindCategoryViewModel(categoryViewModel: CategoryViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CategoryDetailViewModel::class)
    abstract fun bindCategoryDetailViewModel(categoryDetailViewModel: CategoryDetailViewModel): ViewModel

}