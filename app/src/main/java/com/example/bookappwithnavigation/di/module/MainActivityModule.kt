package com.example.bookappwithnavigation.di.module

import com.example.bookappwithnavigation.ui.book.BookFragment
import com.example.bookappwithnavigation.ui.booklist.BookListFragment
import com.example.bookappwithnavigation.ui.categorylist.CategoryFragment
import com.example.bookappwithnavigation.ui.main.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MainActivityModule {

    @ContributesAndroidInjector(
        modules = [
            BookFragmentModule::class,
            CategoryFragmentModule::class
        ]
    )
    abstract fun contributeMainActivity(): MainActivity

}

@Module
abstract class BookFragmentModule {

    @ContributesAndroidInjector(
        modules = [
            BookListFragmentModule::class
        ]
    )
    abstract fun contributeBookFragment(): BookFragment
}

@Module
abstract class BookListFragmentModule {

    @ContributesAndroidInjector
    abstract fun contributeBookListFragmentModule(): BookListFragment

}

@Module
abstract class CategoryFragmentModule {

    @ContributesAndroidInjector
    abstract fun contributeCategoryFragmentModule(): CategoryFragment
}