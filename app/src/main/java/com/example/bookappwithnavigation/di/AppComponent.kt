package com.example.bookappwithnavigation.di

import android.app.Application
import com.example.bookappwithnavigation.App
import com.example.bookappwithnavigation.di.module.ContextModule
import com.example.bookappwithnavigation.di.module.MainActivityModule
import com.example.bookappwithnavigation.di.module.ViewModelModule
import com.example.bookappwithnavigation.di.module.ui.CategoryDetailModule
import com.example.bookappwithnavigation.di.module.ui.CategoryDetailModule_ContributeCategoryEditModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjection
import dagger.android.AndroidInjectionModule
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        AndroidSupportInjectionModule::class,
        MainActivityModule::class,
        ViewModelModule::class,
        CategoryDetailModule::class,
        ContextModule::class
    ]
)


interface AppComponent {

    @Component.Builder
    interface Builder{

        @BindsInstance
        fun context(context:Application):Builder

        fun create(): AppComponent
    }

    fun inject(app: App)

}