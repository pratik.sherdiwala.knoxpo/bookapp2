package com.example.bookappwithnavigation.di.module.ui

import com.example.bookappwithnavigation.ui.categorydetail.CategoryDetailActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class CategoryDetailModule {

    @ContributesAndroidInjector
    abstract fun contributeCategoryEditModule():CategoryDetailActivity

}