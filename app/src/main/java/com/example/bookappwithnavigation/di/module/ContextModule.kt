package com.example.bookappwithnavigation.di.module

import android.app.Application
import android.content.Context
import com.example.bookappwithnavigation.App
import dagger.Module
import dagger.Provides

@Module
class ContextModule {

    @Provides
    fun provideContext(app: Application): Context {
        return app
    }
}